json.array!(@comments) do |comment|
  json.extract! comment, :id, :comment_content, :bug_id
  json.url comment_url(comment, format: :json)
end
