json.array!(@bugs) do |bug|
  json.extract! bug, :id, :bug_title, :bug_description
  json.url bug_url(bug, format: :json)
end
