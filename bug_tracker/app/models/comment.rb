class Comment < ActiveRecord::Base
    belongs_to :bug
    validates(:comment_content, presence: true)
    validates(:bug_id, presence: true)
end
