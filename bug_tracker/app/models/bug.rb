class Bug < ActiveRecord::Base
    has_many :comments
    validates(:bug_title, presence: true, length: {minimum: 5})
    validates(:bug_description, presence: true, length: {minimum: 50})
end
